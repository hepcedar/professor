# -*- python -*-

from professor2.histos import *
from professor2.paramsio import *
from professor2.weights import *
import sys, os.path, glob


def read_histos_root(path):
    "Load histograms from a ROOT file, into a dict of path -> yoda.Histo[DataBin]"
    histos = {}

    try:
        import ROOT
        ROOT.gROOT.SetBatch(True)
    except ImportError:
        print("PyROOT not available... skipping")
        return histos

    # TODO: use yoda.root.getall
    def _getallrootobjs(d, basepath="/"):
        "Recurse through a ROOT file/dir and generate (path, obj) pairs"
        for key in d.GetListOfKeys():
            kname = key.GetName()
            if key.IsFolder():
                # TODO: -> "yield from" in Py3
                for i in _getallrootobjs(d.Get(kname), basepath+kname+"/"):
                    yield i
            else:
                yield basepath+kname, d.Get(kname)
    try:
        f = ROOT.TFile(path)
        for rname, robj in _getallrootobjs(f):
            bins = []
            if robj.InheritsFrom("TH1"):
                # TODO: allow 2D histos
                if robj.InheritsFrom("TH2"):
                    continue
                for ib in range(robj.GetNbinsX()):
                    xmin = robj.GetXaxis().GetBinLowEdge(ib+1)
                    xmax = robj.GetXaxis().GetBinUpEdge(ib+1)
                    y = robj.GetBinContent(ib+1)
                    ey = robj.GetBinError(ib+1)
                    bins.append(DataBin(xmin, xmax, y, ey))
                histos[rname] = Histo(bins, rname)
            elif robj.InheritsFrom("TGraph"):
                for ip in range(robj.GetN()):
                    x, y = ROOT.Double(), ROOT.Double()
                    robj.GetPoint(ip, x, y)
                    xmin = x - robj.GetErrorXlow(ip)
                    xmax = x + robj.GetErrorXhigh(ip)
                    ey = (robj.GetErrorXlow(ip) + robj.GetErrorXhigh(ip)) / 2.0
                    bins.append(DataBin(xmin, xmax, y, ey))
            histos[rname] = Histo(bins, rname)
        f.Close()
    except Exception as e:
        print("Can't load histos from ROOT file '%s': %s" % (path, e))

    return histos


def read_histos_yoda(path, **kwargs):
    "Load histograms from a YODA-supported file type, into a dict of path -> yoda.Histo[DataBin]"
    # TODO:
    histos = {}
    try:
        import yoda
        s2s = []
        # TODO: add filtering regexes and use proactively here
        aos = yoda.read(path, asdict=False, **kwargs)
        #print([ao.path() for ao in aos])

        ## Load all analysis objects and convert to Scatters
        for ao in aos:
            import os
            ## Skip the Rivet cross-section and event counter objects
            # TODO: Avoid Rivet-specific behaviour by try block handling & scatter.dim requirements
            if os.path.basename(ao.path()).startswith("_"):
                continue
            ##
            s2 = ao.mkScatter()
            s2.setPath(ao.path()) #< necessary from YODA 2.0.0
            s2s.append(s2)
        del aos #< pro-active YODA memory clean-up

        ## Convert 2D Scatters (discarding others) to Prof Histo objects
        for s2 in filter(lambda x : x.dim() == 2, s2s):
            #print("*", s2.path())
            bins = [DataBin(p.xMin(), p.xMax(), p.y(), p.yErrAvg()) for p in s2.points()]
            histos[s2.path()] = Histo(bins, s2.path())
        del s2s #< pro-active YODA memory clean-up

    except Exception as e:
        print("Can't load histos from file '%s': %s" % (path, e))
    return histos


def read_histos(filepath, stripref=True, ignoreraw=True):
    """
    Load histograms from file, into a dict of path -> yoda.Histo[DataBin]

    If stripref = True, remove any leading /REF prefix from the histo path
    before putting it in the dictionary.

    If ignoreraw = True, ignore any MC-run histos with a leading /RAW prefix (as these
    are Rivet's pre-finalize duplicates of the final histos.)
    """
    histos = {}
    if filepath.endswith(".root"):
        histos.update(read_histos_root(filepath))
    elif any(filepath.endswith(ext) for ext in [".yoda", ".yoda.gz"]):
        histos.update(read_histos_yoda(filepath))

    ## Remove /RAW histograms
    ## (we could proactively not load them in the first place for YODA...)
    if ignoreraw:
        histos = { k : v for k, v in histos.items() if not k.startswith("/RAW") }
        # for k in histos.keys():
        #     if k.startswith("/RAW"):
        #         del histos[k]

    ## Rewrite /REF histo names
    if stripref:
        newhistos = {}
        for p, h in histos.items():
            if p.startswith("/REF"):
                p = p.replace("/REF", "", 1)
                h.path = p
            newhistos[p] = h
        histos = newhistos

    return histos


def read_weightfile_anames(wfile):
    """
    Return list of analysis names from the weights file, assuming Rivet path conventions.
    """
    pms = read_pointmatchers(wfile)
    paths = [pm.path for pm in pms]

    import rivet
    anames = [rivet.AOPath(path).dirnameparts()[0] for path in paths]
    anames = sorted(list(set(anames))) #< remove duplicates and sort
    return anames


def read_weightfile_refpaths(wfile):
    """
    Return list of YODA file paths for Rivet analyses via the weights file,
    using Rivet's own search mechanism.
    """
    import rivet
    ypaths = []
    for aname in read_weightfile_anames(wfile):
        yname = aname+".yoda"
        ypath = rivet.findAnalysisRefFile(yname) #< auto-checks for .gz variation
        if not ypath:
            print(f"Warning: no YODA file found for analysis {aname}")
        else:
            ypaths.append(ypath)
    return ypaths


def read_selected_histos(filepaths, stripref=True, ignoreraw=True):
    """
    Load histograms from the given files, into a dict of path -> yoda.Histo[DataBin]

    If stripref is true, remove any leading /REF prefix from the histo path
    before putting it in the dictionary.

    If ignoreraw is true, ignore any MC-run histos with a leading /RAW prefix (as these
    are Rivet's pre-finalize duplicates of the final histos.)
    """
    histos = {}
    N = len(filepaths)
    import math
    interval = 10**round(math.log10(N/10)) #< make ~ 20 updates to file
    end = "\n"
    if sys.stdout.isatty():
        interval = 10 if N > 10 else 1 #< faster updates for TTY
        #print('\x1b[7l', end='') # disable line wrap
        end = "\r" # no newline
    lastlen = 0; pad = ""
    for n, fp in enumerate(filepaths):
        if (n+1) % interval == 0 or (n+1) == N:
            msg = f"Reading data file '{fp}': {n+1:d}/{N:d} = {100*(n+1)/N:2.0f}%"
            if sys.stdout.isatty():
                pad = max(0, lastlen - len(msg)) * " " #< manually clear all the last message
            lastlen = len(msg)
            print(msg+pad, end=end)
        histos.update(read_histos(fp, stripref, ignoreraw))
    if sys.stdout.isatty():
        #print('\x1b[7l', end='')
        print() #< clear after the overwriting
    return histos


def read_all_histos(dirpath, stripref=True, ignoreraw=True):
    """
    Load histograms from all data files in the given directory,
    into a dict of path -> yoda.Histo[DataBin]

    If stripref is true, remove any leading /REF prefix from the histo path
    before putting it in the dictionary.

    If ignoreraw is true, ignore any MC-run histos with a leading /RAW prefix (as these
    are Rivet's pre-finalize duplicates of the final histos.)
    """
    filepaths = glob.glob(os.path.join(dirpath, "*"))
    return read_selected_histos(filepaths, stripref, ignoreraw)


def read_weightfile_histos(wfile, stripref=True, ignoreraw=True):
    """
    Load Rivet histograms from all analyses listed in the given weights file,
    into a dict of path -> yoda.Histo[DataBin]

    If stripref is true, remove any leading /REF prefix from the histo path
    before putting it in the dictionary.

    If ignoreraw is true, ignore any MC-run histos with a leading /RAW prefix (as these
    are Rivet's pre-finalize duplicates of the final histos.)
    """
    filepaths = read_weightfile_refpaths(wfile)
    return read_selected_histos(filepaths, stripref, ignoreraw)


def read_rundata(dirs, pfname="params.dat", verbosity=1, ignore_missing_histos=False): #, formats="yoda,yoda.gz,root"):
    """
    Read interpolation anchor point data from a provided set of run directory paths.

    Returns a pair of dicts, the first mapping run names (i.e. rundir basenames) to
    the parameter value list for each run, and the second mapping observable names
    (i.e. histogram paths) to a run -> histo dict.
    """
    params, histos = {}, {}
    import os, glob, re
    re_pfname = re.compile(pfname) if pfname else None
    if sys.stdout.isatty():
        end = "\r" # no newline
    N = len(dirs)
    import math
    interval = 10**round(math.log10(N/10)) #< make ~ 20 updates to file
    end = "\n"
    if sys.stdout.isatty():
        interval = 10 if N > 10 else 1 #< faster updates for TTY
        #print('\x1b[7l', end='') # disable line wrap
        end = "\r" # no newline
    lastlen = 0; pad = ""
    for n, d in enumerate(sorted(dirs)):
        if not os.path.isdir(d):
            continue
        run = os.path.basename(d)
        if (n+1) % interval == 0 or (n+1) == N:
            msg = f"Reading run '{run}' data: {n+1:d}/{N:d} = {100*(n+1)/N:2.0f}%"
            if sys.stdout.isatty():
                pad = max(0, lastlen - len(msg)) * " " #< manually clear all the last message
            lastlen = len(msg)
            print(msg+pad, end=end)
        files = glob.glob(os.path.join(d, "*"))
        found_histos = False
        for f in files:
            ## Params file
            #if os.path.basename(f) == pfname:
            if re_pfname and re_pfname.search(os.path.basename(f)):
                params[run] = read_paramsfile(f)
            ## Potential histo file
            else:
                try:
                    ## Read as a path -> Histo dict
                    hs = read_histos(f)
                    if hs:
                        found_histos = True
                    ## Restructure into the path -> run -> Histo return dict
                    for path, hist in hs.items():
                        histos.setdefault(path, {})
                        if not run in histos[path]:
                            histos[path][run] = hist
                        else:
                            raise Exception("Duplicate histogram '%s' loaded in run dir '%s'" % (path, d))
                except:
                    pass #< skip files that can't be read as histos

        ## Check that a params file was found and read in this dir... or that no attempt was made to find one
        if pfname:
            if run not in params:
                #print()
                raise Exception("No params file '%s' found in run dir '%s'" % (pfname, d))
        else: #< if pfname was null, no attempt was made
            params = None

        ## Check that at least one histogram was found
        if not found_histos:
            #print()
            if ignore_missing_histos:
                print(f"Warning: No histograms found in run dir '{d}'")
            else:
                raise Exception("No histograms found in run dir '%s'" % d)

    if sys.stdout.isatty():
        print() #< end of loop over run dirs, newline needed cf. overwrites
    return params, histos


def read_params(topdir, pfname="params.dat", verbosity=0):
    """
    Read interpolation anchor point data from a provided set of run directory paths.

    Returns a dict mapping run names (i.e. rundir basenames) to
    the parameter value list for each run.
    """
    params = {}
    import os, glob, re
    re_pfname = re.compile(pfname) if pfname else None
    dirs = [x for x in glob.glob(os.path.join(topdir, "*")) if os.path.isdir(x)]
    if sys.stdout.isatty():
        end = "\r"
    N = len(dirs)
    import math
    interval = 10**round(math.log10(N/10)) #< make ~ 20 updates to file
    end = "\n"
    if sys.stdout.isatty():
        interval = 10 if N > 10 else 1 #< faster updates for TTY
        #print('\x1b[7l', end='') # disable line wrap
        end = "\r" # no newline
    lastlen = 0; pad = ""
    for n, d in enumerate(sorted(dirs)):
        run = os.path.basename(d)
        if (n+1) % interval == 0 or (n+1) == N:
            msg = f"Reading run '{run}' params: {n+1:d}/{N:d} = {100*(n+1)/N:2.0f}%"
            if sys.stdout.isatty():
                pad = max(0, lastlen - len(msg)) * " " #< manually clear all the last message
            lastlen = len(msg)
            print(msg+pad, end=end)
        files = glob.glob(os.path.join(d, "*"))
        for f in files:
            ## Params file
            #if os.path.basename(f) == pfname:
            if re_pfname and re_pfname.search(os.path.basename(f)):
                params[run] = read_paramsfile(f)

        ## Check that a params file was found and read in this dir... or that no attempt was made to find one
        if pfname:
            if run not in list(params.keys()):
                raise Exception("No params file '%s' found in run dir '%s'" % (pfname, d))
        else:
            params = None

    if sys.stdout.isatty():
        print() #< end of loop over run dirs, newline needed cf. overwrites
    return params


# TODO this is much slower --- understand why!
# # http://stackoverflow.com/questions/16415156/using-sets-with-the-multiprocessing-module
# def read_rundata(dirs, pfname="params.dat", verbosity=1, nthreads=1): #, formats="yoda,root,aida,flat"):
    # """
    # Read interpolation anchor point data from a provided set of run directory paths.

    # Returns a pair of dicts, the first mapping run names (i.e. rundir basenames) to
    # the parameter value list for each run, and the second mapping observable names
    # (i.e. histogram paths) to a run -> histo dict.
    # """
    # params, histos = {}, {}
    # import os, glob, re
    # re_pfname = re.compile(pfname) if pfname else None
    # import time, multiprocessing
    # time1 = time.time()

    # from multiprocessing.managers import Manager

    # params = {}
    # histos = {}
    # manager = SyncManager()
    # manager.start()
    # histflat = manager.list()

    # # Logic:
    # #
    # #   Only directories containing the uniquely named params file are valid,
    # #   so read the params first and ignore all directories not having one
    # #   of those and then use the runs to prepare structure for multiproc dict
    # #
    # ## The job queue
    # q = multiprocessing.Queue()
    # #
    # for d in dirs:
        # run = os.path.basename(d)
        # files = glob.glob(os.path.join(d, "*"))
        # for f in files:
            # ## Params file
            # if re_pfname and re_pfname.search(os.path.basename(f)):
                # params[run]=read_paramsfile(f)
                # # histos[run]={}
                # q.put(d)



    # import sys

    # def worker(q, rflat):
        # while True:
            # if q.empty():
                # break
            # d = q.get()
            # run = os.path.basename(d)
            # files = glob.glob(os.path.join(d, "*"))
            # for f in files:
                # ## Params file
                # if re_pfname and not  re_pfname.search(os.path.basename(f)):
                    # try:
                        # ## Read as a path -> Histo dict
                        # hs = read_histos(f)
                        # temp=[]
                        # for path, hist in hs.iteritems():
                            # rflat.append([path,run,hist])

                    # except Exception, e:
                        # print e
                        # pass #< skip files that can't be read as histos


    # workers = [multiprocessing.Process(target=worker, args=(q, histflat)) for i in range(nthreads)]
    # map(lambda x:x.start(), workers)
    # map(lambda x:x.join(),  workers)
    # time2 = time.time()
    # sys.stderr.write('\rReading took %0.2fs\n\n' % ((time2-time1)))

    # for p, r, h in histflat:
        # histos.setdefault(p, {})[r] =h

    # time3 = time.time()
    # sys.stderr.write('\rData preparaion took %0.2fs\n\n' % ((time3-time2)))



    # return params, histos


def read_all_rundata(runsdir, pfname="params.dat", verbosity=1, ignore_missing_histos=False):#, nthreads=1):
    rundirs = glob.glob(os.path.join(runsdir, "*"))
    return read_rundata(rundirs, pfname, verbosity, ignore_missing_histos=ignore_missing_histos)#, nthreads)


def read_all_rundata_yaml(yamlfile):
    from professor2.utils import mkdict
    PARAMS = {}
    HISTOS = {}
    import yaml
    print("Loading YAML from %s" % yamlfile)
    Y = yaml.load(open(yamlfile))
    for num, y in enumerate(Y):
        P = mkdict()
        for p in sorted(y['Params'].keys()):
            P[p] = float(y['Params'][p])
        PARAMS[num] = P
        for f in y['YodaFiles']:
            hs = read_histos(f)
            for path, hist in hs.items():
                HISTOS.setdefault(path, {})[num] = hist
    return PARAMS, HISTOS


def find_maxerrs(histos):
    """
    Helper function to find the maximum error values found for each bin in the histos double-dict.

    histos is a nested dict of DataHisto objects, indexed first by histo path and then by
    run name, i.e. it is the second of the two objects returned by read_histos().

    Returns a dict of lists of floats, indexed by histo path. Each list of floats contains the
    max error size seen for each bin of the named observable, across the collection of runs
    histos.keys().

    This functions is useful for regularising chi2 etc. computation by constraining interpolated
    uncertainties to within the range seen in the run data used to create the ipols, to avoid
    blow-up of uncertainties with corresponding distortion of fit optimisation.

    TODO: asymm version?
    """
    rtn = {}
    for hn, hs in histos.items():
        numbins_h = hs.next().nbins
        maxerrs_h = []
        for ib in range(numbins_h):
            emax = max(h.bins[ib].err for h in list(hs.values()))
            maxerrs_h.append(emax)
        rtn[hn] = maxerrs_h
    return rtn


def read_all_rundata_h5(fname):
    """
    Helper/transition function --- read tuning data from hdf5 file and make
    Histo, params structure.
    """
    try:
        import h5py
    except ImportError:
        raise Exception("Module h5py not found --- try pip install h5py")

    with h5py.File(fname, "r") as f:
        binNames = [i.astype(str) for i in f.get("index")]
        pnames   = [p.astype(str) for p in f.get("params").attrs["names"]]
        _P       = [dict(zip(pnames, p)) for p in f.get("params")] # Parameter points as dicts

    import numpy as np
    # Strip the bin numbers and use set to get list of unique histonames
    binNamesStripped = np.array([i.split("#")[0] for i in binNames])
    histoNames = sorted(list(set(binNamesStripped)))

    # dict structure for convenient access of bins by index --- histoname : [indices ...]
    lookup = { hn : np.where(binNamesStripped == hn)[0] for hn in histoNames }

    histos = {}

    with h5py.File(fname, "r") as f:
        # Read y-values and errors
        # NOTE --- first index is the bin index, the second is the run index
        # e.g. Y[33][122] is the y-value of bin 34 in run 123
        Y    = f.get("values")[:]
        E    = f.get("errors")[:]
        xmin = f.get("xmin")[:]
        xmax = f.get("xmax")[:]

        # Useable run indices, i.e. not all things NaN
        USE = np.where(~np.all(np.isnan(Y), axis=0))[0]

        # The usable parameter points
        P = [_P[u] for u in USE]

        for hname in histoNames:
            histos[hname] = {}

            # Connection between histograms and dataset indices
            bindices = lookup[hname]

            # This is an iteration over runs
            for u in USE:
                _Y     = Y[bindices][:,u]
                _E     = E[bindices][:,u]
                _xmin  = xmin[bindices]
                _xmax  = xmax[bindices]
                _bins  = [DataBin(_a,_b,_c,_d) for _a,_b,_c,_d in zip(_xmin,_xmax,_Y,_E)]
                _histo = Histo(_bins, hname)
                histos.setdefault(hname, {})[u] = _histo

    params = { u : p for u, p in zip(USE, P) }

    return params, histos


if __name__ == "__main__":
    import sys
    P, H = read_all_rundata_h5(sys.argv[1])
