# -*- python -*-

def mkFitFunc(fname, pnames, globname, extraargs=[], memfn=False):
    """
    Dynamically make a fit function for the given param names, to be passed to Minuit.

    Return a string definition of the function, to be exec'd, and the list of
    generated internal arg names corresponding to pnames. fname is the internally
    used GoF calculation function, globname is the public-facing one with the param wrapper.

    If memfn=True, generate code for a class member function rather than free function.
    """
    fargs = pnames # ["A%03i" % i for i in range(len(pnames))]
    selfarg = "self, " if memfn else ""
    funcdef = "def {gname}({selfarg}{fargs}):\n    x = lambda {fargs}: {fname}([{fargs}], {extrargs})"\
        .format(gname=globname,
                fargs=", ".join(fargs),
                fname=fname,
                extrargs=", ".join(extraargs),
                selfarg=selfarg)
    funcdef += "\n    return x({fargs})".format(fargs=", ".join(fargs))
    return funcdef
# Alias
mk_fitfunc = mkFitFunc


def mkFitFuncMember(fname, pnames, globname, extraargs=[]):
    """
    Dynamically make a fit function for the given param names, to be passed to Minuit.

    Return a string definition of the function, to be exec'd, and the list of
    generated internal arg names corresponding to pnames.

    DEPRECATED: use mkFitFunc with the memfn=True argument.
    """
    # fargs = ["A%03i" % i for i in range(len(pnames))]
    # funcdef = "def {gname}(self, {fargs}):\n    x = lambda {fargs}: {fname}([{fargs}], {extrargs})"\
    #     .format(gname=globname, fargs=", ".join(fargs), fname=fname, extrargs=", ".join(extraargs))
    # funcdef += "\n    return x({fargs})".format(fargs=", ".join(fargs))
    funcdef = mkFitFunc(fname, pnames, globname, extraargs, memfn=True)
    return funcdef
# Alias
mk_classfitfunc = mkFitFuncMember


def prepareBox(ipolFile, dataHistos, matchers=None, maxErrDict=None, doFilter=False, debug=False):#, normWeights=False):
    import professor2 as prof

    ## Lists of bins to include in the tuning for each analysis name
    include_bins = {}

    ## Read interpolated and reference histos, and run data
    IHISTOS, METADATA = prof.read_ipoldata(ipolFile)
    PNAMES = METADATA["ParamNames"].split()
    if not PNAMES:
        PNAMES = ["A%03i" % i for i in range(int(METADATA["Dimension"]))]
    PMIN   = [float(x) for x in METADATA["MinParamVals"].split()]
    PMAX   = [float(x) for x in METADATA["MaxParamVals"].split()]

    # Prepare tuples to be used as keys in ipol box dicts
    center = tuple([PMIN[i] + 0.5*(PMAX[i]-PMIN[i]) for i in range(len(PMIN))])
    box    = tuple([(PMIN[i], PMAX[i])              for i in range(len(PMIN))])

    ## Find histo objects available in both ipol and ref data (and in matchers if not None)
    available_ihn_rhn = []
    for ihn in sorted(IHISTOS.keys()):
        # ## Set default bin weights
        # for ib in IHISTOS[ihn].bins:
        #     ib.w = 1.0

        ## Find user-specified bin weights if there was a weight file
        if matchers is not None:
            ## Find matches
            pathmatch_matchers = [(m,wstr) for m,wstr in matchers.items() if m.match_path(ihn)]
            ## Ditch histos not listed in the weight file
            if not pathmatch_matchers:
                del IHISTOS[ihn]
                continue

            ## Attach fit weights to the ibins, setting to zero if there's no position match
            ## NB. last match is used if there are multiple
            for ib in IHISTOS[ihn].bins:
                posmatch_matchers = [(m,wstr) for (m,wstr) in pathmatch_matchers if m.match_pos(ib)]
                if posmatch_matchers != []:
                    #print("-->",ib.n)   # debug print
                    include_bins.setdefault(ihn,[]).append(ib.n)

                wstr = posmatch_matchers[-1][1] if posmatch_matchers else "0"
                wparts = wstr.split()
                ib.w = float(wparts[0])
                ## Add optional normalization instructions for the GoF
                if len(wparts) > 1 and "norm" in wparts[1]:
                    ib.norm = f"{wparts[1]}_{ihn}" #< histo ID needed since info is lost in the bin-list

        for rhn in list(dataHistos.keys()):
            if ihn == rhn or rhn == ("/REF/"+ihn): #< TODO: make a helper for this comparison
                # TODO: we should eliminate this potential mismatch of ref and MC hnames
                available_ihn_rhn.append([ihn,rhn])
                break #< TODO: ok? NOT SURE

    # Debug print: bins that are included in the tuning for each analysis
    # for k in sorted(include_bins.keys()):
    #     print(k,":", include_bins[k])

    ## Prepare lists of ibins and dbins
    IBINS, DBINS, MAXERRS, FILTERED = [], [], [], []
    BINDICES = {} # Allows for more helpful error messages in case of prof.StatError
    for iname, rname in available_ihn_rhn:
        if len(IHISTOS[iname].bins) != len(dataHistos[rname].bins):
            print(f"Warning: {iname} inconsistent numebrs of data and parametrised bins")
            print(f"  #data = {len(dataHistos[rname].bins)}, #ipol = {len(IHISTOS[iname].bins)}")
            if iname in include_bins:
                print(f"  Only the following bins will be used: {include_bins[iname][0], include_bins[iname][-1]}")

        ## If a weights file was given and this analysis wasn't listed, skip it
        if include_bins and iname not in include_bins:
            continue

        ## Assemble a list of selected bin indices for this analysis
        BINDICES[iname] = []
        nb = -1
        for ib in IHISTOS[iname].bins:
            nb = nb + 1

            ## If a weights file was given, skip bins not listed in it
            if len(include_bins) > 0:
                if ib.n not in include_bins[iname]:
                    continue
            ## Filter out zero-error bins if requested
            if doFilter and dataHistos[rname].bins[nb].err == 0:
                FILTERED.append(1)
                continue
            ## Only add bins with non-zero weight
            # TODO: zero-weights should be added to output/predictions, but not the fit
            if IHISTOS[iname].bins[nb].w > 0:
                IBINS.append(IHISTOS[iname].bins[nb])
                DBINS.append(dataHistos[rname].bins[ib.n])   # here ib.n is the correct index
                BINDICES[iname].append(len(IBINS)-1)
                # Debug print: to check if the same bin for IHISTOS and dataHistos is taken correctly
                #print("Ipol:",IBINS[-1].xedges, "- Data:", DBINS[-1].xedges)

        if maxErrDict:
            MAXERRS.extend(MAXERRS[iname])

    if debug:
        print("Using the following data-objects:")
        for a in available_ihn_rhn:
            print("Ipol =", iname, "Data =", rname)
    if not MAXERRS:
        MAXERRS = None

    if debug:
        print("DEBUG: filtered %i bins due to zero data error" % len(FILTERED))

    ## Sanity checks
    assert len(IBINS) == len(DBINS)
    if not IBINS:
        raise prof.NoBinsError("No bins left ... exiting")

    assert MAXERRS is None or len(IBINS) == len(MAXERRS)

    PMIN = list(map(float, METADATA["MinParamVals"].split()))
    PMAX = list(map(float, METADATA["MaxParamVals"].split()))
    center = tuple([PMIN[i] + 0.5*(PMAX[i]-PMIN[i]) for i in range(len(PMIN))])
    box    = tuple([(PMIN[i], PMAX[i])              for i in range(len(PMIN))])
    returntuple = (
            ("IBINS",    IBINS),
            ("DBINS",    DBINS),
            ("BINDICES", BINDICES),
            ("MAXERRS",  MAXERRS),
            ("IHISTOS",  IHISTOS),
            ("PNAMES",   PNAMES),
            ("BOX",      box),
            ("CENTER",   center)
            )
    
    return box, center, returntuple

def boxFilt(boxdict, obsnames):
    """
    Make a sub set of a box for a list of observables
    """
    # TODO also use MAXERRS ???
    IBINS, DBINS = [], []
    MAXERRS=None
    BINDICES={}
    IHISTOS={}
    PNAMES = boxdict["PNAMES"]
    box    = boxdict["BOX"]
    center = boxdict["CENTER"]
    for k, v in list(boxdict["BINDICES"].items()):
        if len(v) >0:
            if k in obsnames:
                BINDICES[k] = []
                for nb in v:
                    IBINS.append(boxdict["IBINS"][nb])
                    DBINS.append(boxdict["DBINS"][nb])
                    BINDICES[k].append(len(IBINS)-1)
                IHISTOS[k] = boxdict["IHISTOS"][k]

    return dict(
            (
            ("IBINS",    IBINS),
            ("DBINS",    DBINS),
            ("BINDICES", BINDICES),
            ("MAXERRS",  MAXERRS),
            ("IHISTOS",  IHISTOS),
            ("PNAMES",   PNAMES),
            ("BOX",      box),
            ("CENTER",   center)
            )
            )


def pInBOX(P, box, debug=False):
    for num, p in enumerate(P):
        if debug:
            print(min(box[num]), "<", p, "<", max(box[num]), "?")
        if p<min(box[num]) or p>max(box[num]):
            return False
    return True


def pBoxDistance(A, B):
    import math
    return math.sqrt(sum([ (A[i]-B[i])*(A[i]-B[i]) for i in range(len(A))]))


def sigmoid(x, k=1.0):
    from numpy import exp
    if -k*x>300:
        return 0.0
    return 1.0/(exp(-k*x)+1.0)


def regularizedGoF(params, masterbox, mastercenter, theoErr, debug, unitweights=False):
    """
    simple regularized chi2 loss function defined as dev2/(1+dev2)
    NOTE: Only to be used for global tunes, but very useful for exploration
          of which observables the model can deliver
    """
    from numpy import sqrt
    # Get the right box first
    boxdict=None
    if len(list(masterbox.keys()))==1:
        boxdict=list(masterbox.values())[0]
    else:
        for box, bdict in masterbox.items():
            if pInBOX(params, box, debug):
                boxdict = bdict
                break
        if boxdict is None:
            distances={}
            for c in list(mastercenter.keys()):
                distances[pBoxDistance(params, c)] = c
            winner = min(distances.keys())
            boxdict = mastercenter[distances[winner]]

    ibins    = boxdict["IBINS"]
    dbins    = boxdict["DBINS"]
    maxerrs  = boxdict["MAXERRS"]
    bindices = boxdict["BINDICES"]
    sumLoss = 0.0
    for num, ibin in enumerate(ibins):
        ## Weight is attached to the ipol bin (default set to 1.0 above)
        w = ibin.w
        if w == 0:
            continue
        ## Get ipol & ref bin values and compute their difference
        ival = ibin.val(params)
        dval = dbins[num].val
        ## Data error
        
        ## Plus interpolation error added in quadrature
        maxierr = maxerrs[ibin] if maxerrs else None
        err2 = ibin.err(params, emax=maxierr)**2

        CombinedErr = sqrt(dbins[num].err**2 + (theoErr*dval)**2 + err2)
        # TODO: compute asymm error for appropriate deviation direction cf. sum([e**2 for e in ibin.ierrs])
        # TODO: should we square w too, so it penalised deviations _linearly_?
        dev2 = ((ival-dval)/(CombinedErr))**2
        regularizedLoss = dev2/(1.0+dev2)
        if unitweights:
            w=1.0
        sumLoss += w * w * regularizedLoss
    return sumLoss


def tanhRegularizedGoF(params, masterbox, mastercenter, theoErr, debug, unitweights=False):
    """
    regularized chi2 loss function defined as tanh(dev2)
    looks locally (dev2<<1) the closest to chi2, but not the same interpretation
    NOTE: Only to be used for global tunes, but very useful for exploration
          of which observables the model can deliver
    """
    from numpy import sqrt,tanh
    # Get the right box first
    boxdict=None
    if len(list(masterbox.keys()))==1:
        boxdict=list(masterbox.values())[0]
    else:
        for box, bdict in masterbox.items():
            if pInBOX(params, box, debug):
                boxdict = bdict
                break
        if boxdict is None:
            distances={}
            for c in list(mastercenter.keys()):
                distances[pBoxDistance(params, c)] = c
            winner = min(distances.keys())
            boxdict = mastercenter[distances[winner]]

    ibins    = boxdict["IBINS"]
    dbins    = boxdict["DBINS"]
    maxerrs  = boxdict["MAXERRS"]
    bindices = boxdict["BINDICES"]
    numBinsOutOfXsigma = 0.0
    for num, ibin in enumerate(ibins):
        ## Weight is attached to the ipol bin (default set to 1.0 above)
        w = ibin.w
        if w == 0:
            continue
        ## Get ipol & ref bin values and compute their difference
        ival = ibin.val(params)
        dval = dbins[num].val
        
        ## Plus interpolation error added in quadrature
        maxierr = maxerrs[ibin] if maxerrs else None
        err2 = ibin.err(params, emax=maxierr)**2

        # square add theory percentage error to data error and interpolation error
        CombinedErr = sqrt(dbins[num].err**2 + (theoErr*dval)**2 + err2)
        # TODO: compute asymm error for appropriate deviation direction cf. sum([e**2 for e in ibin.ierrs])
        # TODO: should we square w too, so it penalised deviations _linearly_?
        dev2=((ival-dval)/(CombinedErr))**2
        smoothed=tanh(dev2)
        if smoothed<0.0 or smoothed>1.0:
            print("ERROR")
            print(smoothed)
        if unitweights:
            w=1.0
        numBinsOutOfXsigma += w * w * smoothed
    return numBinsOutOfXsigma

def sigmoidGoF(params, masterbox, mastercenter, theoErr, debug, unitweights=False):
    """
    GOF defined as sigmoid activation function withing some numSigmas(hardcoded for now)
    of stddevs. Probably not the best since locally (dev2<<1) not much similar to chi2
    NOTE: Only to be used for global tunes, but very useful for exploration
          of which observables the model can deliver.
          Note also the hardcoded hyperparameters K,numSigmas
    """
    from numpy import sqrt
    # Get the right box first
    boxdict=None
    if len(list(masterbox.keys()))==1:
        boxdict=list(masterbox.values())[0]
    else:
        for box, bdict in masterbox.items():
            if pInBOX(params, box, debug):
                boxdict = bdict
                break
        if boxdict is None:
            distances={}
            for c in list(mastercenter.keys()):
                distances[pBoxDistance(params, c)] = c
            winner = min(distances.keys())
            boxdict = mastercenter[distances[winner]]

    ibins    = boxdict["IBINS"]
    dbins    = boxdict["DBINS"]
    maxerrs  = boxdict["MAXERRS"]
    bindices = boxdict["BINDICES"]
    # TODO maybe pass as parameter and not hardcode
    numSigmas=1.0
    # TODO maybe pass as parameter and not hardcode
    K=5.0
    numBinsOutOfXsigma = 0.0
    for num, ibin in enumerate(ibins):
        ## Weight is attached to the ipol bin (default set to 1.0 above)
        w = ibin.w
        if w == 0:
            continue
        ## Get ipol & ref bin values and compute their difference
        ival = ibin.val(params)
        dval = dbins[num].val

        # square add theory percentage error to data error
        CombinedErr2 = (dbins[num].err**2 + (theoErr*dval)**2)
        ## Plus interpolation error added in quadrature
        maxierr = maxerrs[ibin] if maxerrs else None
        err2 = ibin.err(params, emax=maxierr)**2

        CombinedErr = sqrt(CombinedErr2 + err2)
        # TODO: compute asymm error for appropriate deviation direction cf. sum([e**2 for e in ibin.ierrs])
        Xlow=(ival-(dval-numSigmas*CombinedErr))/CombinedErr
        XHig=(ival-(dval+numSigmas*CombinedErr))/CombinedErr
        # 1- theta function but smeared
        smoothed=(1.0-sigmoid(Xlow,k=K)*sigmoid(-XHig,k=K))
        if smoothed<0.0 or smoothed>1.0:
            print("ERROR")
            print(smoothed)
        if unitweights:
            w=1.0
        numBinsOutOfXsigma += w * w * smoothed
    return numBinsOutOfXsigma


def GoF_chi2(params, masterbox, mastercenter, theoErr, debug, unitweights=False):
    """
    Very straightforward goodness-of-fit measure chi2 
    """
    import professor2 as prof
    ## Choose overload for GoF option
    chi2 = 0.0

    ## Get the right box first
    boxdict = None
    if len(list(masterbox.keys())) == 1:
        boxdict=list(masterbox.values())[0]
    else:
        for box, bdict in masterbox.items():
            if pInBOX(params, box, debug):
                boxdict = bdict
                break
        if boxdict is None:
            distances = {}
            for c in list(mastercenter.keys()):
                distances[pBoxDistance(params, c)] = c
            winner = min(distances.keys())
            boxdict = mastercenter[distances[winner]]

    ## Extract the fit data
    ibins    = boxdict["IBINS"]
    dbins    = boxdict["DBINS"]
    maxerrs  = boxdict["MAXERRS"]
    bindices = boxdict["BINDICES"]

    ## Precalculate ipol and data norms for scaling
    dnorms, inorms = {}, {}
    for num, ibin in enumerate(ibins):
        if ibin.w == 0:
            continue
        if ibin.norm:
            if ibin.norm in inorms and ibin.norm.startswith("norm1"): #< norm to match 1st bin only
                continue
            dbin = dbins[num]
            ival = ibin.val(params) #< TODO: avoid calling this twice...
            dval = dbin.val
            if ibin.norm.startswith("normA"): #< area norm
                ival *= ibin.xwidth
                dval *= dbin.xwidth
            inorms.setdefault(ibin.norm, 0.0)
            dnorms.setdefault(ibin.norm, 0.0)
            inorms[ibin.norm] += ival
            dnorms[ibin.norm] += dval
    ## Convert to multiplicative scale factors on the ibins
    iscales = {}
    for normstr in dnorms.keys():
        #print(normstr, dnorms[normstr], inorms[normstr])
        iscales[normstr] = dnorms[normstr] / inorms[normstr]
        #print(iscales[normstr])

    ## Compute chi2
    for num, ibin in enumerate(ibins):
        ## Weight is attached to the ipol bin (default set to 1.0 above)
        if ibin.w == 0:
            continue
        dbin = dbins[num]
        ## Get ipol bin value and error
        ival = ibin.val(params)
        maxierr = maxerrs[ibin] if maxerrs else None
        ierr = ibin.err(params, emax=maxierr)
        ## Scale the ival and ierr if normed
        if ibin.norm:
            sf = iscales[ibin.norm]
            ival *= sf
            ierr *= sf
        ## Compare the (scaled) value to the ref data
        dval = dbin.val
        diff = dval - ival
        ## Compute total uncertainty: data error + interpolation error + theory error (all in quadrature, latter wrt data val)
        # TODO: compute asymm error for appropriate deviation direction cf. sum([e**2 for e in ibin.ierrs])
        err2 = dbin.err**2
        err2 += ierr**2
        err2 += (theoErr*dval)**2
        ## Check the uncertainty, identify problems
        if not err2:
            culprit = ""
            i_culprit = -1
            for k, v in bindices.items():
                if num in v:
                    culprit = k
                    i_culprit = v.index(num)
            raise prof.StatError("Zero uncertainty on a bin being used in the fit -- cannot compute a reasonable GoF!"
                                 "  Observable: %s"
                                 "  %s %f+=%f"
                                 "  %s"
                                 "  See weight-syntax in documentation or user --filter CL arg to remove bins with zero data error automatically" % \
                                 (culprit, ibin, ival, ibin.err(params, emax=maxierr),  dbin))
        if unitweights:
            chi2 += diff**2 / err2
        else:
            chi2 += ibin.w**2 * diff**2 / err2
    return chi2
def GoF_chi2_xsecWeighted(params, masterbox, mastercenter, theoErr, debug, unitweights=False):
    """
    Goodness-of-fit measure chi2 weighted with the probability of each bin
        i.e. weight for bin b of histogram H is w_{b,H}/(sum {b in histogram H} w_{b,H})
        with w_b = binwidth(b) * data_value(b)
        NOTE: A global observable weight will be automatically set automatically as the first
              non-zero weight of the histogram H
        NOTE: In principle ranged weights are allowed, but the global observable weight will 
              be still set to the first non-zero weight
    """
    if debug:
        print("WARNING: for GOFFUNCTION=chi2xsec only observable weights must be used, not individual bin or range weights"
              "NOTE: A global observable weight will be automatically set automatically as the first"
              "      non-zero weight of the histogram H"
              "NOTE: In principle ranged weights are allowed, but the global observable weight will "
              "      be still set to the first non-zero weight")

    import professor2 as prof
    # choose overload for GoF option
    chi2 = 0.0
    # Get the right box first
    boxdict=None
    if len(list(masterbox.keys()))==1:
        boxdict=list(masterbox.values())[0]
    else:
        for box, bdict in masterbox.items():
            if pInBOX(params, box, debug):
                boxdict = bdict
                break
        if boxdict is None:
            distances={}
            for c in list(mastercenter.keys()):
                distances[pBoxDistance(params, c)] = c
            winner = min(distances.keys())
            boxdict = mastercenter[distances[winner]]

    ibins    = boxdict["IBINS"]
    dbins    = boxdict["DBINS"]
    maxerrs  = boxdict["MAXERRS"]
    bindices = boxdict["BINDICES"]

    for observable, indices in bindices.items():
        wObsSum = 0.0
        chi2ObsSum = 0.0
        wObs = 0.0 
        ObservableIgnored = True
        for num in indices:
            ## Weight is attached to the ipol bin (default set to 1.0 above)
            w = ibins[num].w
            if w == 0:
                continue
            ObservableIgnored = False
            # hacky: set observable weight to first non-zero bin weight
            if wObs == 0.0:
                wObs = w
            ## Get ipol & ref bin values and compute their difference
            ival = ibins[num].val(params)
            dval = dbins[num].val
            diff = dval - ival
            ## Data error
            err2 = dbins[num].err**2
            ## Plus interpolation error added in quadrature
            maxierr = maxerrs[ibins[num]] if maxerrs else None
            err2 += ibins[num].err(params, emax=maxierr)**2
            # square add theory percentage error
            err2 += (theoErr*dval)**2
            # TODO: compute asymm error for appropriate deviation direction cf. sum([e**2 for e in ibinnum.ierrs])
            if not err2:
                raise prof.StatError("Zero uncertainty on a bin being used in the fit -- cannot compute a reasonable GoF!\n\tObservable: %s\n\t%s %f+=%f\n\t%s \nSee weight-syntax in documentation or user --filter CL arg to remove bins with zero data error automatically"%(observable, ibins[num], ival, ibins[num].err(params, emax=maxierr),  dbins[num]))
            # for single bin histograms set binwidth to 1 and probability xsec to 1
            xsec = 1.0
            dx = 1.0
            if len(indices)>1:
                # set single dx to binwidth
                dx = (dbins[num].xmax - dbins[num].xmin) 
                if dx <= 0:
                    print(f"ERROR: dx = {dx} <= 0. This should never happen")
                # only allow xsec weighting with positive definite data value
                # else default to default weighting
                if dval <= 0:
                    print(f"ERROR: dval = {dval} <= 0 for observable {observable}. Set global weight to 1.0")
                    print(f"NOTE:  You might want to ignore non-xsec related observable {observable}")
                    xsec = 1.0
                else:
                    xsec = dx*dval
                    if xsec <=0:
                        print(f"WARNING:                      dx = {dx} \t dval = {dval} xsec = {xsec}")
            #  else:
                #  print("Single Bin histo")
            if unitweights:
                w=1.0
            weight =  w*xsec # dval*dx = data probability to fall in that bin (unnormalized)
            if weight <= 0:
                print(f"weight = {weight} for {observable} {num}")
            wObsSum += weight
            chi2ObsSum += weight * diff**2 / err2
        if wObsSum == 0:
            if len(indices)>0 and not ObservableIgnored:
                print(f"WARNING: wObsSum over {observable} is {wObsSum}")
                print(f"WARNING: chi2ObsSum over {observable} is {chi2ObsSum}")
                print(f"WARNING: w = {w} \t dx = {dx} \t dval = {dval} xsec = {xsec}")
                print(f"WARNING: indices = {indices}")
            continue
        chi2+=wObs*chi2ObsSum/wObsSum
    return chi2


def simpleGoF(params, masterbox, mastercenter, GoFoption, theoErr, debug, unitweights=False):
    """
    Choose GOF/Loss function
    """
    # choose overload for GoF option
    #  print(f"using GoF function {GoFoption}")
    if GoFoption=="tanhChi2":
        return tanhRegularizedGoF(params, masterbox, mastercenter, theoErr, debug, unitweights=False)
    elif GoFoption=="chi2":
        return GoF_chi2(params, masterbox, mastercenter, theoErr, debug, unitweights=False)
    elif GoFoption=="chi2xsec":
        return GoF_chi2_xsecWeighted(params, masterbox, mastercenter, theoErr, debug, unitweights=False)
    elif GoFoption=="sigmoidChi2":
        return sigmoidGoF(params, masterbox, mastercenter, theoErr, debug, unitweights=False)
    elif GoFoption=="chiRegularized":
        return regularizedGoF(params, masterbox, mastercenter, theoErr, debug, unitweights=False)
    else :
        print(f"ERROR: Not found GoF function {GoFoption}")
        assert(false)


def mkMinuitParamsDict(pnames, pstart, pmins, pmaxs, limits, fixed, allowExtrapolation=False, verbose=True):
    ## Dictionary fitarg for iminuit
    farg = dict()

    ## Initial conditions --- use pos = center of hypercube, and step = range/10
    assert len(pmins) == len(pmaxs)

    pranges = [(pmaxs[i] - pmins[i]) for i in range(len(pmins))]

    # This sets the start point
    for i, aname in enumerate(pnames):
        farg[aname] = pstart[i]
        #farg['error_%s' % aname] = pranges[i] / 10.

    # for i, pname in enumerate(pnames):
    #     if not allowExtrapolation:
    #         if verbose:
    #             print("Setting minimiser limit to interpolation limit for '%s', you can use -x to allow extrapolation" % pname)
    #         farg['limit_%s' % pname] = (pmins[i],pmaxs[i])
    #     if pname in list(limits.keys()):
    #         farg['limit_%s' % pname] = limits[pname]
    #     if pname in list(fixed.keys()):
    #         farg[pname] = fixed[pnames[i]]
    #         farg['fix_%s' % pname] = True

    return farg

#Alias
setupMinuitFitarg = mkMinuitParamsDict


def readResult(fname):
    """
       Open results file, extract and return minimum point as OrderedDict
       and return raw list of all other lines for further processing.
    """
    RES=[]
    OTH=[]
    with open(fname) as f:
        for line in f:
            l=line.strip()
            if l.startswith("#"):
                OTH.append(l)
            else:
                temp=l.split()
                RES.append([temp[0], float(temp[1])])

    from collections import OrderedDict
    return OrderedDict(RES), OTH

def getParamCov(TXT):
    """
       Read the covariance matrix from the lines, return as numpy array
    """
    START = TXT.index("# Covariance matrix:") + 2
    dim = len(TXT[START].strip().split()) - 2
    END = START+dim
    COV_raw = TXT[START:END]
    COV_txt = [COV_raw[d].split()[2:2+dim] for d in range(dim)]

    # Go through line by line and find fixed params
    fixed=[]
    for num, c in enumerate(COV_txt):
        if all([x=='---' for x in c]):
            fixed.append(num)

    # This is the reduced cov matrix
    COV_l = []
    for num, c in enumerate(COV_txt):
        if not num in fixed:
            cline = [float(x) for num2, x in enumerate(c) if not num2 in fixed]
            COV_l.append(cline)

    # Cast it into a numpy array
    from numpy import zeros
    COV_p = zeros((dim-len(fixed), dim-len(fixed)))
    for i in range(dim-len(fixed)):
        for j in range(dim-len(fixed)):
            COV_p[i][j] = COV_l[i][j]


    return COV_p

def getFixedParams(TXT):
    """
       Read fixed parameters
    """
    from collections import OrderedDict
    fixed=OrderedDict()
    START = TXT.index("# Fixed:") + 1
    for line in TXT[START:]:
        l=line[1:].strip().split()
        if len(l)==0:
            return fixed
        else:
            fixed[l[0]]=float(l[1])

def getParamLimits(TXT):
    """
       Read parameters limits
    """
    from collections import OrderedDict
    fixed=OrderedDict()
    START = TXT.index("# Limits:") + 1
    for line in TXT[START:]:
        l=line[1:].strip().split()
        if len(l)==0:
            return fixed
        else:
            fixed[l[0]]=(float(l[1]), float(l[2]))

def getParamCorr(TXT):
    """
       Read the corrlation matrix from the lines, return as numpy array
    """
    START = TXT.index("# Correlation matrix:") + 2
    dim = len(TXT[START].strip().split()) - 2
    END = START+dim
    COV_raw = TXT[START:END]
    from numpy import zeros
    COV_p = zeros((dim, dim))
    for i in range(dim):
        temp = list(map(float, COV_raw[i].split()[2:2+dim]))
        for j in range(dim):
            COV_p[i][j] = temp[j]


    return COV_p

def eigenDecomposition(mat):
    """
    Given a symmetric, real NxN matrix, M, an eigen decomposition is always
    possible, such that M can be written as M = T_transp * S * T (orthogonal
    transformation) with T_transp * T = 1_N and S being a diagonal matrix with
    the eigenvalues of M on the diagonal.

    Returns
    -------
    T_trans : numpy.matrix
    S : numpy.ndarray
        The real eigen values.
    T : numpy.matrix
    """
    from scipy import linalg
    from numpy import matrix
    import numpy

    A = matrix(mat)
    S, T_trans = linalg.eig(A)
    if numpy.iscomplex(S).any():
        raise ValueError("Given matrix `mat` has complex eigenvalues!")

    return matrix(T_trans), S.real, matrix(T_trans).transpose()

def mkEigentunes(COV, point, plus=True):
    T_trans, S, T = eigenDecomposition(COV)
    from numpy import sqrt, zeros, matrix
    rv = matrix(list(point.values()))
    rv_trans = (T_trans * rv.transpose()).transpose()

    ret = []

    for num, c in enumerate(S):
        ev = zeros(len(S))
        sigma=sqrt(c)
        if plus:
            ev[num] =sigma
        else:
            ev[num] = -1* sigma
        ev_trans = rv_trans + ev
        etune_params_t = T * ev_trans.transpose()
        etune_params = etune_params_t.transpose().tolist()[0]
        ret.append([sigma, etune_params])

    return ret
